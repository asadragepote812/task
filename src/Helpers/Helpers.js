export const webHost = 'reqres.in';
export const URI = __DEV__ ?'':'';
// export const ImageUrl =__DEV__ ?'https://'+webHost:'';
const axios = require('axios');
export const FetchGet = (PATH,access_token='') => {
    const config = {
        headers: {Authorization: `Bearer ${access_token}`}
    }
    return axios.get(URI+ PATH, config).then(response => response.data).catch(err =>err.response);
}
export const FetchPost = (PATH,formData,access_token='') =>{
    const config = {
        headers: {Authorization: `Bearer ${access_token}`}
    }
    return axios.post(URI+ PATH,formData, config).then(response => response.data).catch(err =>err.response);
}
export const colors = {
    red: '#FF3B30',
    orange: '#FF9500',
    yellow: '#FFCC00',
    green: '#4CD964',
    tealBlue: '#5AC8FA',
    blue: '#007AFF',
    purple: '#5856D6',
    pink: '#FF2D55',
    white: '#FFFFFF',
    customGray: '#EFEFF4',
    lightGray: '#E5E5EA',
    lightGray2: '#D1D1D6',
    midGray: '#C7C7CC',
    gray: '#8E8E93',
    black: '#000000',
    back_ground_color:'#141215'
}



