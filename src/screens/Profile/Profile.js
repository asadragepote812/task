import React from 'react';
import {View,Image,FlatList,ScrollView} from 'react-native';
import styles from './styles';
import {  Header, Left, Body, Right, Button, Icon, Title,Text,Tab, Tabs, TabHeading,Footer,FooterTab } from 'native-base';
import ShowFolowInfo from "../../components/ShowFolowInfo/ShowFolowInfo";
import {colors} from "../../Helpers/Helpers";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import Stories from "../../components/Stories/Stories";
import {get_Stores_data} from "./controller";
import ProfileImages from "../../components/ProfileImages/ProfileImages";
import LiveVideoShares from "../../components/LiveVideoShares/LiveVideoShares";
class Profile extends React.Component{

    componentDidMount() {
        get_Stores_data().then((res) =>{
            if(res.results){
                this.setState({
                    stories:res.results
                })
            }
        })
    }

    state = {
        stories :[]
    }
    renderStores = ({item}) =>{
    const {name,picture} =item;
        return(
            <Stories
                imageUri={picture.large}
                story_text={name.first+' '+name.last} />
        )

    }
    renderPosts = ({item}) =>{
        const {name,picture} =item;
        return(
            <LiveVideoShares  imageUri={picture.large} />
        )

    }
    renderProfileImages = ({item}) =>{
        const {name,picture} =item;
        return(
            <ProfileImages
                imageUri={picture.large}
                story_text={name.first+' '+name.last} />
        )

    }
    render() {
        const {stories} = this.state;
        return (
            <View style={styles.container}>
                <Header transparent>
                    <Left>
                        <Button transparent>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Apple</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            <Icon name='menu' />
                        </Button>
                    </Right>
                </Header>
                <ScrollView>
                <View style={styles.top_view_outer}>
                <View style={styles.profile_view}>
                    <Image
                        source={{uri:'https://randomuser.me/api/portraits/men/38.jpg'}}
                        style={styles.image_style}
                        />
                </View>
                 <View style={styles.right_outer_view}>
                    <ShowFolowInfo
                        count={'20'}
                        bottonText={'Posts'}
                    />
                     <ShowFolowInfo
                         count={'24.3 M'}
                         bottonText={'Followers'}
                     />
                     <ShowFolowInfo
                         count={6}
                         bottonText={'Following'}
                     />
                 </View>
                </View>
                <View style={styles.page_about_info}>
                    <Text style={styles.page_about_info_text}>
                        apple
                    </Text>
                    <Text style={styles.page_about_info_text}>
                        Everyone has a story to tell
                    </Text >
                    <Text style={styles.page_about_info_text}>
                       Tag #shoutin to take a part.
                    </Text>
                    <Text style={styles.page_about_info_text}>
                        One Apple Park Way, Cupertiniz, California
                    </Text>
                </View>
                <View style={styles.buttons_outer_view}>
                    <Button  style={styles.button_style}  iconRight bordered light>
                        <Text uppercase={false}  style={[styles.button_inner_text,{paddingRight:wp('1%')}]}>Following</Text>
                        <Icon style={{fontSize:20}} name='ios-chevron-down' color={colors.white} />
                    </Button>
                    <Button style={styles.button_style}  bordered light>
                        <Text uppercase={false}  style={styles.button_inner_text}>Message</Text>
                    </Button>
                    <Button style={styles.arrow_button} bordered light>
                        <Icon  name='ios-chevron-down' style={{marginRight:0,}} color={colors.white} />
                    </Button>
                </View>
                <View style={styles.stories_outer_view}>
                    <FlatList
                        horizontal={true}
                        data={stories}
                        renderItem={this.renderStores}
                        keyExtractor={({item}) =>Math.random().toString()}
                    />
                </View>
                <View style={styles.bottom_tabs_outer_view}>
                    <Tabs >
                        <Tab heading={ <TabHeading  style={{backgroundColor:colors.back_ground_color}} ><Icon name="ios-apps-outline" /></TabHeading>}>
                           <View style={styles.tab_outer_view}>
                               <FlatList
                                   nestedScrollEnabled
                                   data={stories}
                                   numColumns={3}
                                   renderItem={this.renderProfileImages}
                                   keyExtractor={({item}) =>Math.random().toString()}
                               />
                           </View>
                        </Tab>
                        <Tab heading={ <TabHeading style={{backgroundColor:colors.back_ground_color}} ><Icon color={colors.white} name="camera" /></TabHeading>}>
                            <View style={styles.tab_outer_view}>
                                <FlatList
                                    nestedScrollEnabled
                                    data={stories}
                                    numColumns={2}
                                    renderItem={this.renderPosts}
                                    keyExtractor={({item}) =>Math.random().toString()}
                                />
                            </View>
                        </Tab>
                        <Tab heading={ <TabHeading style={{backgroundColor:colors.back_ground_color}} ><Icon color={colors.white} name="apps" /></TabHeading>}>
                            <View style={styles.tab_outer_view}>
                                <FlatList
                                    nestedScrollEnabled
                                    data={stories}
                                    numColumns={3}
                                    renderItem={this.renderProfileImages}
                                    keyExtractor={({item}) =>Math.random().toString()}
                                />
                            </View>
                        </Tab>
                    </Tabs>
                </View>
                </ScrollView>
                <Footer >
                    <FooterTab style={{backgroundColor:colors.back_ground_color}}>
                        <Button vertical>
                            <Icon name="apps" />
                            <Text>Apps</Text>
                        </Button>
                        <Button vertical>
                            <Icon name="camera" />
                            <Text>Camera</Text>
                        </Button>
                        <Button vertical>
                            <Icon  name="navigate" />
                            <Text>Navigate</Text>
                        </Button>
                        <Button vertical>
                            <Icon name="person" />
                            <Text>Contact</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </View>
        );
    }
}
export default Profile;