import {StyleSheet} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from "../../Helpers/Helpers";

const styles = StyleSheet.create({
    container:{
        backgroundColor:colors.back_ground_color,
        flex:1
    },
    top_view_outer:{
        // flex: 1,
        flexDirection:'row',
        justifyContent: 'center',
        alignContent:'center',
        alignItems:'center',
        paddingVertical:wp('3%'),
        paddingHorizontal:wp('1.5%')
    },
    profile_view:{
        flex:1,
    },
    right_outer_view:{
        flex:3,
        flexDirection: 'row',
        justifyContent:'space-around',
    },
    image_style:{
        height:wp('20'),
        width:wp('20'),
        borderRadius:wp('10%')
    },
    page_about_info:{
        flex:1
    },
    page_about_info_text:{
        color:colors.white,
        paddingLeft:wp('3%'),
        fontWeight: '100'
    },
    buttons_outer_view:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-around',
        paddingHorizontal:wp('1.5%'),
        paddingTop:wp('3%')
    },
    button_inner_text:{
        color:colors.white
    },
    button_style:{
        height: wp('8%'),
        paddingHorizontal: wp('8%'),
        borderRadius:wp('1%')
    },
    arrow_button:{
        height: wp('8%'),
        justifyContent:'center',
        alignItems: 'center',
        alignContent: 'center',
        paddingHorizontal:wp('1.5%'),
        borderRadius:wp('1%')
    },
    stories_outer_view:{
        flex:2,
        flexDirection:'row',
        paddingHorizontal:wp('1.5%'),
        paddingVertical: wp('3%')
    },
    bottom_tabs_outer_view:{
        // flex:1,
        flexDirection:'row',
        paddingHorizontal:wp('1.5%')
    },
    tab_outer_view:{
        maxHeight:hp('71%'),
        backgroundColor: colors.back_ground_color
    },


});
export default styles;