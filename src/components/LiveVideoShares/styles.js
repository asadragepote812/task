import {StyleSheet} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from "../../Helpers/Helpers";


const styles = StyleSheet.create({
    image_posts:{
        height:hp('45%'),
        width:wp('47%'),
        borderRadius:wp('2%')
    },
    show_views_text:{
        color:colors.white,
        fontWeight:'100',
        fontSize:wp('3.5%')
    },
    show_title_text:{
        color:colors.white,
        fontWeight:'bold',
        fontSize:wp('5%')
    },
    video_descrtion_outer:{
        position:'absolute',
        bottom:wp('3%'),
        left:wp('4%')
    },
    show_title_text_outer:{
        width: wp('35%')
    }

});
export default styles;