import React from 'react'
import styles from "./styles";
import {View,Text,Image} from "react-native";
import {colors} from "../../Helpers/Helpers";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";

const LiveVideoShares = (props) =>{
    const {imageUri} = props;
    return(
        <View style={{padding:wp('1%')}}>
            <Image
                source={{uri:imageUri}}
                style={styles.image_posts}
            />
            <View style={styles.video_descrtion_outer}>
                <View style={styles.show_title_text_outer}>
                    <Text style={styles.show_title_text} >
                            Making My Task
                    </Text>
                </View>
                <View style={{marginTop:wp('3%')}}>
                    <Text  style={styles.show_views_text} >
                            418K views
                    </Text>
                </View>
            </View>
        </View>
    )
}
export default LiveVideoShares;