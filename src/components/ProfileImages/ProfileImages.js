import React from 'react'
import styles from "./styles";
import {View,Text,Image} from "react-native";
import {colors} from "../../Helpers/Helpers";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";

const ProfileImages = (props) =>{
    const {imageUri} = props;
    return(
        <View style={{padding:wp('.2%')}}>
            <Image
                source={{uri:imageUri}}
                style={styles.image_posts}
            />
        </View>
    )
}
export default ProfileImages;