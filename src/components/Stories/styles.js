import {StyleSheet} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {colors} from "../../Helpers/Helpers";


const styles = StyleSheet.create({

    profile_view:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
        marginRight:wp('3%'),

        // borderTopWidth:1
    },
    image_style:{
        height:wp('20'),
        width:wp('20'),
        borderRadius:wp('10%')
    },
    bottom_text:{
        color:colors.white,
        paddingTop:wp('1%')
    },
    storie_circle:{
        borderRadius:wp('10%'),
        padding:wp('.4%'),
        borderColor:colors.white,
        borderWidth:1,
    }


});
export default styles;