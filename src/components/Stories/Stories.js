import React from 'react'
import styles from "./styles";
import {View,Text,Image} from "react-native";
import {colors} from "../../Helpers/Helpers";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";


const Stories = (props) =>{
    const {imageUri,story_text} = props;
    return(
        <View style={styles.profile_view}>
            <View style={styles.storie_circle}>
                <Image
                    source={{uri:imageUri}}
                    style={styles.image_style}
                />
            </View>

            <Text style={styles.bottom_text}>
                {story_text}
            </Text>
        </View>
    )
}
export default Stories;