import React from 'react';
import {View,Text} from "react-native";
import styles from "./styles";

const ShowFolowInfo = (props) =>{
    const {count,bottonText} = props;
    return(
        <View style={styles.right_views}>
            <Text style={styles.follow_count}>
                {count}
            </Text>
            <Text style={styles.follow_text}>
                {bottonText}
            </Text>
        </View>
    )
}
export default  ShowFolowInfo;