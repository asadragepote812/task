import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {colors} from "../../Helpers/Helpers";

const styles = StyleSheet.create({
    follow_count:{
        color:'white',
        fontSize:wp('4%'),
        fontWeight:'bold'
    },
    follow_text:{
        color:colors.white,
        fontWeight: '100'
    },
    right_views:{
        justifyContent:'center',
        alignContent:'center',
        alignItems: 'center'
    },
})
export default styles;